# Sample Web app deploy with Gitlab CI on GCP
## Introduction


The objective of this project is to explain the deployment of a web app developed with nodeJS, with a mysql database; and a backend API; both of it conteneraized and deployed in a VM hosted in Google Cloud Platform. To see the proyect deployed, you can access clicking the following link. 


[http://34.71.154.74.nip.io:3050/](http://34.71.154.74.nip.io:3050/)


[Spanish version](./documentation_ES/readme-ES.md)


## Sample Web APP


The web app is based on [Moses Maina's](https://www.section.io/engineering-education/authors/mos-maina/) [project](https://www.section.io/engineering-education/authors/moses-maina/), who developed a React base web app to review books as an example to dockerizar components and thus be able to deploy easily with docker-compose.

The site has a web app developed in React, a backend API developed in nodeJS, a mySQL database, an Adminer database manager and an nginx as a reverse proxy to handle web traffic.


## VM in Google Cloud Platform

For this project, a virtual machine instance was created in [Google Cloud Platform](https://console.cloud.google.com/compute/instances)


The equivalent VM command used:

El comando equivalente de la VM utilizada:
```bash
gcloud compute instances create instance-1 --project=books-reviewer-343916 --zone=us-central1-a --machine-type=e2-medium --network-interface=network-tier=PREMIUM,subnet=default --maintenance-policy=MIGRATE --service-account=939095022541-compute@developer.gserviceaccount.com --scopes=https://www.googleapis.com/auth/devstorage.read_only,https://www.googleapis.com/auth/logging.write,https://www.googleapis.com/auth/monitoring.write,https://www.googleapis.com/auth/servicecontrol,https://www.googleapis.com/auth/service.management.readonly,https://www.googleapis.com/auth/trace.append --tags=http-server --create-disk=auto-delete=yes,boot=yes,device-name=instance-1,image=projects/ubuntu-os-cloud/global/images/ubuntu-2004-focal-v20220308,mode=rw,size=10,type=projects/books-reviewer-343916/zones/us-central1-a/diskTypes/pd-balanced --no-shielded-secure-boot --shielded-vtpm --shielded-integrity-monitoring --reservation-affinity=any
```

> The command does not include the public key that is used for the container of the deploy to communicate with the VM. 

After creating it, we proceeded to create the [firewall rules](https://console.cloud.google.com/networking/firewalls) to allow connections on port 3050 (web app client) and 8000 (Adminer)

Then you had access to VM using SSH to install docker and docker-compose and then allow docker to run without root privileges.

## Gitlab CI

To automate the deployment, a . gitlab-ci.yml was created with the following jobs:


* Linter - Verify dockerfiles created for client, server (API) and nginx
* Package - Create the images from the dockerfiles and upload them into a registry so that they can then be used in the deployment of the VM
* Deploy - Deploy using the docker-compose command within the VM by communicating via SSH


The deployment sought to achieve an automated management of the versions to avoid having to change the versions in several files. Only by editing the versions of the package.json for client and server or editing the nginx version in the Dockerfile FROM, the pipeline gets the values to use throughout the deployment.


```yaml
before_script:
  - export CLIENT_BUILD_VERSION=$(cat ./client/package.json | grep \"version\" | cut -d "\"" -f4)
  - export SERVER_BUILD_VERSION=$(cat ./server/package.json | grep \"version\" | cut -d "\"" -f4)
  - export NGINX_BUILD_VERSION=$(cat ./nginx/Dockerfile | grep FROM | cut -d ":" -f2)
  - echo -e "CLIENT_BUILD_VERSION=${CLIENT_BUILD_VERSION}\nSERVER_BUILD_VERSION=${SERVER_BUILD_VERSION}\nNGINX_BUILD_VERSION=${NGINX_BUILD_VERSION}" > .env
  - cat .env
```

Values are exported in variables that are used in the command

```yaml
    - docker build --no-cache -f client/Dockerfile -t ${CI_REGISTRY_IMAGE}:client-${CLIENT_BUILD_VERSION} ./client
    - docker push ${CI_REGISTRY_IMAGE}:client-${CLIENT_BUILD_VERSION}
```


and also kept in an artifact .env so that the docker-compose is modified with the version:


```yaml
api:
    image: registry.gitlab.com/nicolasbacacoria/sample-web-app:server-${SERVER_BUILD_VERSION}
```

> All sensitive variables used by the pipeline are stored in GitLab’s CI/CD environment variables.

## Improvements

Although this repository is based on a project already developed to get up with docker-compose, the following changes were made to achieve a better product:
* Fixed an error that occurred during client compile when the image is deployed.
* The deployment method was modified in the docker-compose.yaml to adapt it to the objective sought by this project.
* Added persistence to database to avoid.

## Project status

Project in development.
