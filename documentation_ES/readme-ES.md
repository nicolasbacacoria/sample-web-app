# Ejemplo de despliegue de Web app con Gitlab CI en GCP
## Introducción


El objetivo de este proyecto es explicar el despliegue de una web app desarrollada con nodeJS, que conecta con una base de datos mysql de manera contenereizada en una VM alojada en Google Cloud Platform y que puede accederse a través de la siguiente dirección: 


[http://34.71.154.74.nip.io:3050/](http://34.71.154.74.nip.io:3050/)


## Sample Web APP


La web app esta basada en el [proyecto](https://www.section.io/engineering-education/authors/moses-maina/) de [Moses Maina](https://www.section.io/engineering-education/authors/moses-maina/) quien desarrollo una página de carga de reseñas de libros como ejemplo para dockerizar los componentes y así poder desplegar fácilmente con docker-compose.


La página cuenta con una web app desarrollada en nodeJS, una API backend también desarrollada en nodeJS, una base de datos mySQL, un Adminer como manejador de base de datos y un nginx como proxy reverso para manejar el trafico web.


## VM en Google Cloud Platform
Para este proyecto se creó una instancia de máquina virtual en [Google Cloud Platform](https://console.cloud.google.com/compute/instances)


El comando equivalente de la VM utilizada:
```bash
gcloud compute instances create instance-1 --project=books-reviewer-343916 --zone=us-central1-a --machine-type=e2-medium --network-interface=network-tier=PREMIUM,subnet=default --maintenance-policy=MIGRATE --service-account=939095022541-compute@developer.gserviceaccount.com --scopes=https://www.googleapis.com/auth/devstorage.read_only,https://www.googleapis.com/auth/logging.write,https://www.googleapis.com/auth/monitoring.write,https://www.googleapis.com/auth/servicecontrol,https://www.googleapis.com/auth/service.management.readonly,https://www.googleapis.com/auth/trace.append --tags=http-server --create-disk=auto-delete=yes,boot=yes,device-name=instance-1,image=projects/ubuntu-os-cloud/global/images/ubuntu-2004-focal-v20220308,mode=rw,size=10,type=projects/books-reviewer-343916/zones/us-central1-a/diskTypes/pd-balanced --no-shielded-secure-boot --shielded-vtpm --shielded-integrity-monitoring --reservation-affinity=any
```
> En el comando no se incluye la llave publica que se utiliza para que el container del deploy se comunique con la VM. 


Luego de crearla, se procedió a crear las [reglas de firewall](https://console.cloud.google.com/networking/firewalls) para permitir conexiones al puerto 3050 (web app client) y 8000 (Adminer)

Después se debió ingresar a la VM por ssh para instalar docker y docker-compose y luego permitir que docker corra sin necesidad de privilegios de superusuario.

## Gitlab CI

Para automatizar el despliegue se creó un .gitlab-ci.yml con los siguientes jobs:


* Linter - Verificar los dockerfiles creados para cliente, server (API) y nginx
* Package - Crea las imágenes a partir de los dockerfiles y las sube a una registry para que luego puedan ser utilizadas en el despliegue de la VM
* Deploy - Despliega utilizando el comando docker-compose dentro de la VM comunicándose a traves de SSH


En el despliegue se buscó lograr un manejo automatizado de las versiones para evitar tener que cambiar las versiones en varios archivos. Solo editando las versiones de los package.json para client y server o editando la versión de nginx en el FROM del Dockerfile, el pipeline obtiene los valores para utilizar en todo el despliegue.


```yaml
before_script:
  - export CLIENT_BUILD_VERSION=$(cat ./client/package.json | grep \"version\" | cut -d "\"" -f4)
  - export SERVER_BUILD_VERSION=$(cat ./server/package.json | grep \"version\" | cut -d "\"" -f4)
  - export NGINX_BUILD_VERSION=$(cat ./nginx/Dockerfile | grep FROM | cut -d ":" -f2)
  - echo -e "CLIENT_BUILD_VERSION=${CLIENT_BUILD_VERSION}\nSERVER_BUILD_VERSION=${SERVER_BUILD_VERSION}\nNGINX_BUILD_VERSION=${NGINX_BUILD_VERSION}" > .env
  - cat .env
```

Los valores se exportan en variables que se utilizan en el comando

```yaml
    - docker build --no-cache -f client/Dockerfile -t ${CI_REGISTRY_IMAGE}:client-${CLIENT_BUILD_VERSION} ./client
    - docker push ${CI_REGISTRY_IMAGE}:client-${CLIENT_BUILD_VERSION}
```


y además se mantienen en un artefacto .env para que el docker-compose se modifique con la versión:


```yaml
api:
    image: registry.gitlab.com/nicolasbacacoria/sample-web-app:server-${SERVER_BUILD_VERSION}
```

> Todas las variables sensibles que utiliza el pipeline se encuentran guardadas en las variables de entorno CI/CD de GitLab.

## Mejoras

Si bien este repositorio está basado en un proyecto ya desarrollado para levantarse con docker-compose, se realizaron los siguientes cambios para lograr un mejor producto:
* Se corrigió un error que ocurría durante la compilación cuando se despliega la imagen del cliente.
* Se modificó el método de despliegue en el docker-compose.yaml para adaptarlo al objetivo buscado por este proyecto.
* Se agregó persistencia a la base de datos para evitar 

## Estado del proyecto

En desarrollo.
